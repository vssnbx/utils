package com.pom_utils.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class GitCmdUtil {

	public static void resetFileList(List<String> fileList) throws Exception {
		for (String file : fileList) {
			resetFileList(file);
		}
	}

	public static int resetFileList(String fileFullPath) throws IOException {
		File file = new File(fileFullPath);
		File workingDir = file.getParentFile(); 
		int exitcode = -1; 
				
		ProcessBuilder processBuilder = new ProcessBuilder();
		ProcessBuilder command = processBuilder.command("git.exe", "checkout", file.getName());
		processBuilder.directory(workingDir);
		Process process = processBuilder.start();
		
		BufferedReader bufferedReader = new BufferedReader((new InputStreamReader(process.getInputStream())));
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			System.out.println(line);
		}
			
		try {
			exitcode = process.waitFor();
			System.out.printf("[INFO] resetPom: [%1$s] exitcode=%2$s\n", fileFullPath, exitcode);
		} catch (Exception e) {
			System.out.printf("[ERROR] resetPom: [%1$s] exitcode=%2$s\n", fileFullPath, exitcode);
			e.printStackTrace();
		}
		
		return exitcode;
	}
	
	/*
	private static class CmdOutput implements Runnable {

		private InputStream inputStream;
		private Consumer<String> consumer;
		
		public CmdOutput(InputStream inputStream, Consumer<String> consumer) {
			this.inputStream = inputStream;
			this.consumer = consumer;
		}

		@Override
		public void run() {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			Stream<String> linesStream = bufferedReader.lines();
			linesStream.forEach(consumer);
		}
		
	}
	
	public static int resetFileList(String fileFullPath) {
		File file = new File(fileFullPath);
		File workingDir = file.getParentFile(); 
		int exitcode = -1; 
				
		try {
			ProcessBuilder processBuilder = new ProcessBuilder();
			ProcessBuilder command = processBuilder.command("git.exe", "checkout", file.getName());
			processBuilder.directory(workingDir);
			Process process = processBuilder.start();
			
			CmdOutput cmdOutput = new CmdOutput(process.getInputStream(), System.out::println);
			ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
			Future<?> future = newSingleThreadExecutor.submit(cmdOutput);
			while (!future.isDone()) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			exitcode = process.waitFor();
			System.out.printf("[INFO] resetPom::exitcode=%1$s\n", exitcode);
		} catch (Exception e) {
			System.out.printf("[ERROR] resetPom::exitcode=%1$s\n", exitcode);
			e.printStackTrace();
		}
		
		return exitcode;
	}
	*/

	
//	public static void resetFileList(List<String> fileList) throws Exception {
//		ExecutorService newSingleThreadExecutor = Executors.newFixedThreadPool(10);
//		for (String pom : fileList) {
//			Process process = resetPom(pom);
//			Future<?> future = newSingleThreadExecutor.submit(cmdOutput);
//		}
//		
//		
//		while (!future.isDone()) {
//			try {
//				Thread.sleep(10);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
//		
//		exitcode = process.waitFor();
//		System.out.printf("INFO resetPom::exitcode=%1$s", exitcode);
//	} catch (Exception e) {
//		System.out.printf("ERROR resetPom::exitcode=%1$s", exitcode);
//		e.printStackTrace();
//	}
//	
//	return exitcode;
//		
//	}
	
//	private static Process resetPom(String fileFullPath) throws Exception {
//		ProcessBuilder processBuilder = new ProcessBuilder();
//		processBuilder.command("git.exe", "checkout", "pom.xml");
//		processBuilder.directory(new File(fileFullPath).getParentFile());
//		Process process = processBuilder.start();
//		
//		CmdOutput cmdOutput = new CmdOutput(process.getInputStream(), System.out::println);
//		return process;
//	}
	
}
