package com.requests;

import org.apache.commons.lang3.StringUtils;

public class RequestDto {

	private String requestUrl;
	
	private String dataBinary;

	public String getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	public String getDataBinary() {
		return dataBinary;
	}

	public void setDataBinary(String dataBinary) {
		this.dataBinary = dataBinary;
	}

	@Override
	public String toString() { 
		String requestUrlTemp = indent(requestUrl);  
		String dataBinaryTemp = indent(dataBinary);
		
		StringBuilder sb = new StringBuilder();
		sb
			.append(requestUrlTemp)
			.append("\n")
			.append("\trequest")
			.append("\n")
			.append(null != dataBinaryTemp ? "\t\t" + dataBinaryTemp : "")
			.append("\n")
			.append("\tresponse")
		;
		return sb.toString();
	}

	private String indent(String value) {
		String attribute;
		
		attribute = "\"proposta\""               ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"idProposta\""             ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"isencaoTAB\""             ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"isencaoTC\""              ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"isencaoTAB\""             ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"isencaoTC\""              ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"fgIsencaoTAB\""           ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"fgIsencaoTC\""            ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"fgPgtoTAB\""              ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"fgPgtoTC\""               ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"cetJson\""                ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"indicadorIsencaoTAB\""    ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"indicadorPgtoAvistaTAB\"" ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"indicadorIsencaoTC\""     ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "\"indicadorPgtoAvistaTC\""  ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);		
		attribute = "fgIsencaoTAB="              ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);		
		attribute = "fgIsencaoTC="               ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);		
		attribute = "fgPgtoTAB="                 ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);		
		attribute = "fgPgtoTC="                  ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);		
		attribute = "idProposta="                ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);
		attribute = "dadosIsencao"               ; value = StringUtils.replaceAll(value, attribute, "\n\t\t"+attribute);		
		
		return value;
	}
	
}
