#!/bin/bash
git config --system core.longpaths true

SUBDIR=$(date +%Y-%m-%d_%H-%M-%S_%N)
OUTPUTDIR=$(pwd)
OUTPUTDIR="$OUTPUTDIR/$SUBDIR"
if [ ! -d "$OUTPUTDIR" ]; then
    echo "Criando $OUTPUTDIR..."
    mkdir "$OUTPUTDIR"
fi



###############################################################################
printf  "Criando $OUTPUTDIR/bpo..."
mkdir "$OUTPUTDIR/bpo"
cd    "$OUTPUTDIR/bpo"


echo "PROJETO [1] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/bpo/backend/analise-doc-financeira.git


###############################################################################
printf  "Criando $OUTPUTDIR/client..."
mkdir "$OUTPUTDIR/client"
cd    "$OUTPUTDIR/client"


echo "PROJETO [2] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/aftersale-client.git
echo "PROJETO [3] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/asynchronous-status-client.git
echo "PROJETO [4] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/clearsale-client.git
echo "PROJETO [5] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/client-santander.git
echo "PROJETO [6] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/consistency.model-client.git
echo "PROJETO [7] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/emailage-client.git
echo "PROJETO [8] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/emailsender-client.git
echo "PROJETO [9] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/gitlab-client.git
echo "PROJETO [10] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/gravame-client.git
echo "PROJETO [11] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/kafka-client.git
echo "PROJETO [12] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/legacy-client.git
echo "PROJETO [13] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/notifications-client.git
echo "PROJETO [14] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/occurence-client.git
echo "PROJETO [15] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/phase-client.git
echo "PROJETO [16] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/rpc-client.git
echo "PROJETO [17] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/token-client.git
echo "PROJETO [18] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/client/user-client.git


###############################################################################
printf  "Criando $OUTPUTDIR/component..."
mkdir "$OUTPUTDIR/component"
cd    "$OUTPUTDIR/component"


echo "PROJETO [19] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/component/capture-utils.git
echo "PROJETO [20] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/component/encryptor-utils.git
echo "PROJETO [21] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/component/jwt-authorize.git
echo "PROJETO [22] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/component/jwt-component.git
echo "PROJETO [23] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/component/profile-component.git
echo "PROJETO [24] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/component/spreadsheet-utils.git


###############################################################################
printf  "Criando $OUTPUTDIR/infraestrutura_vivere..."
mkdir "$OUTPUTDIR/infraestrutura_vivere"
cd    "$OUTPUTDIR/infraestrutura_vivere"


echo "PROJETO [25] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/infraestrutura_vivere/financeira-cloud-config.git


###############################################################################
printf  "Criando $OUTPUTDIR/infrastructure..."
mkdir "$OUTPUTDIR/infrastructure"
cd    "$OUTPUTDIR/infrastructure"


echo "PROJETO [26] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/infrastructure/api-gateway.git
echo "PROJETO [27] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/infrastructure/financeira-cloud.git
echo "PROJETO [28] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/infrastructure/financeira-cloud-config.git
echo "PROJETO [29] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/infrastructure/financeira-dependencies.git
echo "PROJETO [30] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/infrastructure/financeira-dependencies.client.git
echo "PROJETO [31] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/infrastructure/financeira-dependencies.data.git
echo "PROJETO [32] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/infrastructure/financeira-dependencies.log4j2.git
echo "PROJETO [33] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/infrastructure/financeira-dependencies.logback.git
echo "PROJETO [34] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/infrastructure/financeira-dependencies.service.git
echo "PROJETO [35] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/infrastructure/infrastructure.git
echo "PROJETO [36] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/infrastructure/shinken-config.git


###############################################################################
printf  "Criando $OUTPUTDIR/legacy..."
mkdir "$OUTPUTDIR/legacy"
cd    "$OUTPUTDIR/legacy"


echo "PROJETO [37] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/legacy/vivere-app.cdc.git
echo "PROJETO [38] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/legacy/vivere-app.cdc.batch.git
echo "PROJETO [39] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/legacy/vivere-app.cdc.cobol.git
echo "PROJETO [40] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/legacy/vivere-app.cdc.db.git
echo "PROJETO [41] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/legacy/vivere-app.cdc.estilo.git
echo "PROJETO [42] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/legacy/vivere-cdc.git
echo "PROJETO [43] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/legacy/vivere-documento.git
echo "PROJETO [44] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/legacy/vivere-eai.cdc.git
echo "PROJETO [45] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/legacy/vivere-ws.core.git


###############################################################################
printf  "Criando $OUTPUTDIR/microservices..."
mkdir "$OUTPUTDIR/microservices"
cd    "$OUTPUTDIR/microservices"


echo "PROJETO [46] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/address-service.git
echo "PROJETO [47] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/aftersale-service.git
echo "PROJETO [48] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/assignment-consumer.git
echo "PROJETO [49] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/asynchronous-status-service.git
echo "PROJETO [50] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/atx-service.git
echo "PROJETO [51] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/atx.assets.cdc-service.git
echo "PROJETO [52] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/auth-service.git
echo "PROJETO [53] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/billet-service.git
echo "PROJETO [54] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/bordero-service.git
echo "PROJETO [55] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/cancel-service.git
echo "PROJETO [56] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/capture.aftersale-service.git
echo "PROJETO [57] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/capture.assets-service.git
echo "PROJETO [58] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/capture.assets.cdc-service.git
echo "PROJETO [59] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/capture.vehicle-service.git
echo "PROJETO [60] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/certified.agents-service.git
echo "PROJETO [61] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/clearsale-service.git
echo "PROJETO [62] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/compose.attachment-consumer.git
echo "PROJETO [63] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/conciliation-service.git
echo "PROJETO [64] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/contract-consumer.git
echo "PROJETO [65] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/contract-service.git
echo "PROJETO [66] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/customer-service.git
echo "PROJETO [67] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/customer.ib-service.git
echo "PROJETO [68] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/dados-comp-tab-service.git
echo "PROJETO [69] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/denatran-service.git
echo "PROJETO [70] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/document.anticheat-service.git
echo "PROJETO [71] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/domains-service-jpa.git
echo "PROJETO [72] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/ek9-service.git
echo "PROJETO [73] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/emailage-service.git
echo "PROJETO [74] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/emailsender-service.git
echo "PROJETO [75] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/financeira-importfile.service.git
echo "PROJETO [76] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/formalization-service.git
echo "PROJETO [77] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/formalization.aftersale-service.git
echo "PROJETO [78] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/gravame-consumer.git
echo "PROJETO [79] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/gravame-service.git
echo "PROJETO [80] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/identification-service.git
echo "PROJETO [81] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/identification.assets-service.git
echo "PROJETO [82] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/identification.assets.cdc-service.git
echo "PROJETO [83] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/identification.vehicle-service.git
echo "PROJETO [84] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/importfile-service.git
echo "PROJETO [85] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/installment-service.git
echo "PROJETO [86] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/internetbanking-service.git
echo "PROJETO [87] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/kafka-producer-service.git
echo "PROJETO [88] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/konecta.consumer-service.git
echo "PROJETO [89] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/konecta.producer-service.git
echo "PROJETO [90] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/login-service.git
echo "PROJETO [91] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/minuta.git
echo "PROJETO [92] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/notifications.aftersale-service.git
echo "PROJETO [93] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/occurence.ccw-recorder.git
echo "PROJETO [94] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/panel-service.git
echo "PROJETO [95] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/payment-service.git
echo "PROJETO [96] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/phase-service.git
echo "PROJETO [97] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/pipefy-service.git
echo "PROJETO [98] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/proposal-refresher-service.git
echo "PROJETO [99] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/proposal-service.git
echo "PROJETO [100] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/scheduler-consumer.git
echo "PROJETO [101] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/signature-service.git
echo "PROJETO [102] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/simulation-consumer-service.git
echo "PROJETO [103] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/simulation-data.git
echo "PROJETO [104] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/simulation-service.git
echo "PROJETO [105] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/simulation.assets-service.git
echo "PROJETO [106] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/simulation.assets.cdc-service.git
echo "PROJETO [107] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/simulation.vehicle-service.git
echo "PROJETO [108] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/sms-consumer.git
echo "PROJETO [109] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/sms.token-service.git
echo "PROJETO [110] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/solicitation-consumer.git
echo "PROJETO [111] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/solicitation-service.git
echo "PROJETO [112] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/status-service.git
echo "PROJETO [113] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/tab-service.git
echo "PROJETO [114] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/token-service.git
echo "PROJETO [115] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/user-service.git
echo "PROJETO [116] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/vivere-token.git
echo "PROJETO [117] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/vivere-token.consumer.git
echo "PROJETO [118] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/vivere-ws.ticket.git
echo "PROJETO [119] DE [162]"
git clone http://gitlab.intranet/bra/santander/financeira/microservices/vivere-ws.validar-conta.git
