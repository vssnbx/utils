#!/bin/bash

# PROJECTSDIR=$(pwd)
PROJECTSDIR="C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700"
MAVENDIR="C:/opt/maven/apache-maven-3.5.3-bin/conf"

MAVENSETTINGS="$MAVENDIR/settings_legado.xml"
#MAVENSETTINGS="$MAVENDIR/settings_microServices.xml"

LOGPREFIX=`date +%F-%H_%M_%S`
LOGFILE="$PROJECTSDIR/$LOGPREFIX-SWITCH.log"

###############################################################################
switch_branches()
{
	PROJECTDIR="$1"
	
	BRANCH1=develop
	BRANCH2=mesahyundai
	
	cd   "$PROJECTDIR"
	echo "###############################################################################" >> "$LOGFILE"
	echo "$PROJECTDIR" >> "$LOGFILE"
	echo "###############################################################################" >> "$LOGFILE"

	for PROJECT in `ls -1 -d */`
	do
		echo "$PROJECTDIR/$PROJECT" >> "$LOGFILE"
		cd   "$PROJECTDIR/$PROJECT"

		# git checkout $BRANCH1
		# git checkout $BRANCH2
		
		# git pull --progress -v --no-rebase  #>> "$LOGFILE"
		
		# mvn -T 8               install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
		  mvn -T 8               install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
		# mvn -T 8 clean package install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
	done
}
###############################################################################

# switch_branches "$PROJECTSDIR/infraestrutura_vivere"

# switch_branches "$PROJECTSDIR/infrastructure"
switch_branches "$PROJECTSDIR/component"
# switch_branches "$PROJECTSDIR/microservices"
# switch_branches "$PROJECTSDIR/client"
# switch_branches "$PROJECTSDIR/legacy"
# switch_branches "$PROJECTSDIR/bpo"
