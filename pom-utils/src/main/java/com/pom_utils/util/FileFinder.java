package com.pom_utils.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.StringUtils;

public class FileFinder implements Callable<List<String>> {
	
	private File dir;
	private String fileName;

	public FileFinder(File dir, String fileName) {
		this.dir = dir;
		this.fileName = fileName;
	}
	
	@Override
	public List<String> call() throws Exception {
		List<String> pomList = new ArrayList<>();
		setPomList(pomList, dir);
		return pomList;
	}

	private void setPomList(List<String> pomList, File dir) {
		File[] listFiles = dir.listFiles();
		for (int i = 0; i < listFiles.length; i++) {
			File fileTemp = listFiles[i];
			String fileTempAbsolutePath = fileTemp.getAbsolutePath();
			
			if (fileTemp.isDirectory()) {
				
				if (   StringUtils.containsIgnoreCase(fileTempAbsolutePath, ".metadata"        ) 
					|| StringUtils.containsIgnoreCase(fileTempAbsolutePath, ".recommenders"    )
					|| StringUtils.containsIgnoreCase(fileTempAbsolutePath, ".settings"        )
					|| StringUtils.containsIgnoreCase(fileTempAbsolutePath, ".git"             )
					|| StringUtils.containsIgnoreCase(fileTempAbsolutePath, "META-INF"         )
					|| StringUtils.containsIgnoreCase(fileTempAbsolutePath, "target"           )
					|| StringUtils.containsIgnoreCase(fileTempAbsolutePath, "vivere-app.cdc.db")
					|| StringUtils.containsIgnoreCase(fileTempAbsolutePath, "\\src\\test"    )) {
					continue;
				}
				
				//subDirsList.add(subDir);
				setPomList(pomList, fileTemp);
			} else if (fileTemp.getName().equals(fileName)) {
				pomList.add(fileTempAbsolutePath);
			}
		}
	}
}
