#!/bin/bash

# PROJECTSDIR=$(pwd)
PROJECTSDIR="C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700"
MAVENDIR="C:/opt/maven/apache-maven-3.5.3-bin/conf"

#MAVENSETTINGS="$MAVENDIR/settings_legado.xml"
MAVENSETTINGS="$MAVENDIR/settings_microServices.xml"

LOGPREFIX=`date +%F-%H_%M_%S`
#LOGFILE="$PROJECTSDIR/$LOGPREFIX-SWITCH.log"
LOGFILE="$PROJECTSDIR/mvn.log"

###############################################################################
# mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "../$(basename $PWD)-$LOGPREFIX.log"

cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/component/capture-utils/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/component/encryptor-utils/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/component/jwt-authorize/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/component/jwt-component/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/component/profile-component/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/component/spreadsheet-utils/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"

cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/api-gateway/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/financeira-cloud/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/financeira-dependencies.client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/financeira-dependencies.data/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/financeira-dependencies.log4j2/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/financeira-dependencies.logback/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/financeira-dependencies.service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/financeira-dependencies/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/infrastructure/admin-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/infrastructure/config-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/infrastructure/eureka-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/infrastructure/hystrix-dashboard/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/infrastructure/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/infrastructure/infrastructure/zipkin-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"

cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/address-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/aftersale-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/assignment-consumer/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/asynchronous-status-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/atx-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/atx.assets.cdc-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/auth-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/billet-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/bordero-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/cancel-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/capture.aftersale-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/capture.assets-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/capture.assets.cdc-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/capture.vehicle-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/certified.agents-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/clearsale-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/compose.attachment-consumer/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/conciliation-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/contract-consumer/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/contract-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/customer-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/denatran-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/document.anticheat-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/domains-service-jpa/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/ek9-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/emailage-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/emailsender-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/financeira-importfile.service/importfile-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/formalization-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/formalization.aftersale-service/bin/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/formalization.aftersale-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/gravame-consumer/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/gravame-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/identification-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/identification.assets-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/identification.assets.cdc-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/identification.vehicle-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/importfile-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/installment-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/kafka-producer-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/konecta.consumer-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/konecta.producer-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/login-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/minuta/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/notifications.aftersale-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/occurence.ccw-recorder/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/panel-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/payment-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/phase-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/pipefy-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/proposal-refresher-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/proposal-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/scheduler-consumer/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/signature-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/simulation-consumer-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/simulation-data/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/simulation-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/simulation.assets-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/simulation.assets.cdc-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/simulation.vehicle-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/sms-consumer/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/sms.token-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/solicitation-consumer/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/solicitation-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/status-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/tab-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/token-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/user-service/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/vivere-token.consumer/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/vivere-token/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/vivere-ws.ticket/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/microservices/vivere-ws.validar-conta/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"

cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/aftersale-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/asynchronous-status-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/clearsale-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/client-santander/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/consistency.model-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/emailage-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/emailsender-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/gitlab-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/gravame-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/kafka-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/legacy-client/bin/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/legacy-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/notifications-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/occurence-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/phase-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/rpc-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/token-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/client/user-client/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"

cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/bpo/analise-doc-financeira/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"

cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-app.cdc.batch/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-app.cdc.batch/vivere-batch.pan/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-app.cdc.batch/vivere-batch.santander/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-app.cdc.cobol/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-app.cdc.estilo/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-app.cdc/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-app.cdc/vivere-app.cdc.servicos/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-app.cdc/vivere-app.cdc.web/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-cdc/cdc.integracao/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-cdc/cdc.servicos/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-cdc/cdc.web/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-cdc/pocs/boleto/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-cdc/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-documento/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-eai.cdc/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-eai.cdc/vivere-eai.cdc.dto/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-eai.cdc/vivere-eai.cdc.pan/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-eai.cdc/vivere-eai.cdc.santander/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-eai.cdc/vivere-eai.cdc.sicredi/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-eai.cdc/vivere-eai.cdc.tools/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-ws.core/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-ws.core/vivere-comum/comum.documento/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-ws.core/vivere-comum/comum.modeloconsistencia/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-ws.core/vivere-comum/comum.shared/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-ws.core/vivere-comum/comum.workflow/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-ws.core/vivere-comum/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-ws.core/vivere-ws.integracao/captura.integracao/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-ws.core/vivere-ws.integracao/captura.servicos/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-ws.core/vivere-ws.integracao/captura.web/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
cd C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/legacy/vivere-ws.core/vivere-ws.integracao/
mvn -T 8 install -DskipTests --settings "$MAVENSETTINGS" >> "$LOGFILE"
