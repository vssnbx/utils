#!/bin/bash

# PROJECTSDIR=$(pwd)
PROJECTSDIR="C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700"
MAVENDIR="C:/opt/maven/apache-maven-3.5.3-bin/conf"

# microservices =========================================================================

cd "$PROJECTSDIR/component"
git clone  http://gitlab.intranet/bra/santander/financeira/component/jwt-component.git
git clone  http://gitlab.intranet/bra/santander/financeira/component/profile-component.git

cd "$PROJECTSDIR/component/jwt-component"
git checkout mesahyundai
git pull --progress -v --no-rebase "origin"
mvn clean package install -DskipTests --settings "$MAVENDIR/settings_legado.xml"

cd "$PROJECTSDIR/component/profile-component"
git checkout mesahyundai
git pull --progress -v --no-rebase "origin"
mvn clean package install -DskipTests --settings "$MAVENDIR/settings_legado.xml"

# legacy ================================================================================

# cd "$PROJECTSDIR"
# mkdir legacy
# cd legacy
# git clone  http://gitlab.intranet/bra/santander/financeira/legacy/vivere-app.cdc.git
# git clone  http://gitlab.intranet/bra/santander/financeira/legacy/vivere-app.cdc.db.git
# git clone  http://gitlab.intranet/bra/santander/financeira/legacy/vivere-cdc.git
# git clone  http://gitlab.intranet/bra/santander/financeira/legacy/vivere-app.cdc.batch.git
# git clone  http://gitlab.intranet/bra/santander/financeira/legacy/vivere-eai.cdc.git
# git clone  http://gitlab.intranet/bra/santander/financeira/legacy/vivere-app.cdc.cobol.git
# git clone  http://gitlab.intranet/bra/santander/financeira/legacy/vivere-app.cdc.estilo.git
# git clone  http://gitlab.intranet/bra/santander/financeira/legacy/vivere-documento.git
# git clone  http://gitlab.intranet/bra/santander/financeira/legacy/vivere-ws.core.git

cd "$PROJECTSDIR/legacy/vivere-eai.cdc"
# git checkout mesahyundai
# git pull --progress -v --no-rebase "origin"
mvn clean package install -DskipTests --settings "$MAVENDIR/settings_legado.xml"

cd "$PROJECTSDIR/legacy/vivere-app.cdc"
# git checkout mesahyundai
# git pull --progress -v --no-rebase "origin"
mvn clean package install -DskipTests --settings "$MAVENDIR/settings_legado.xml"

cd "$PROJECTSDIR/legacy/vivere-cdc"
# git checkout mesahyundai
# git pull --progress -v --no-rebase "origin"
mvn clean package install -DskipTests --settings "$MAVENDIR/settings_legado.xml"

cd "$PROJECTSDIR/legacy/vivere-app.cdc.batch"
# git checkout mesahyundai
# git pull --progress -v --no-rebase "origin"
mvn clean package install -DskipTests --settings "$MAVENDIR/settings_legado.xml"
