package com.pom_utils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.io.FileUtils;

import com.pom_utils.util.FileFinder;
import com.pom_utils.util.GitCmdUtil;
import com.pom_utils.util.PomYmlFileUtils;;

public class PomYmlUtils {
	
	//private static final String ROOT_DIR_INPUT = "C:/opt/workspaces/workspace01/_levantamento/2018-06-12/microservices/";
	private static final String ROOT_DIR_INPUT = "C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/";

	
	public static void main(String[] args) throws Exception {
		System.out.println("[INI - PomYmlUtils]");
		
		PomYmlUtils pomUtils = new PomYmlUtils();
		pomUtils.execute(ROOT_DIR_INPUT);
		
		System.out.println("[FIM - PomYmlUtils]");
	}

	private void execute(String rootDirInput) throws Exception {
		File fileRootDirInput = new File(rootDirInput);
		String preffix = "2018-08-27-14-57-21-897000000";//getBackupSuffix();

		processPomList(fileRootDirInput, preffix);
//		processYmlList(fileRootDirInput, preffix);
	}
	
	private void processPomList(File fileRootDirInput, String preffix) throws Exception {
		File pomListFile = new File("c:/temp/pomList.txt");
		List<String> pomList = new ArrayList<>();
		if (!pomListFile.exists()) {
			pomList = executeFileFinder(fileRootDirInput, "pom.xml");
			Collections.sort(pomList);
			PomYmlFileUtils.writeList(pomListFile.getAbsolutePath(), pomList);
		} else {
			PomYmlFileUtils.readList(pomListFile, pomList);
		}

//		backupFileList(pomList, preffix);
		
		GitCmdUtil.resetFileList(pomList);
	}
	
	private void processYmlList(File fileRootDirInput, String preffix) throws Exception {
		File ymlListFile = new File("c:/temp/ymlList.txt");
		List<String> ymlList = new ArrayList<>();
		if (!ymlListFile.exists()) {
			ymlList = executeFileFinder(fileRootDirInput, "ap.yml");
			Collections.sort(ymlList);
			PomYmlFileUtils.writeList(ymlListFile.getAbsolutePath(), ymlList);
		} else {
			PomYmlFileUtils.readList(ymlListFile, ymlList);
		}

//		backupFileList(ymlList, preffix);
		
		GitCmdUtil.resetFileList(ymlList);
	}
	
	private List<String> executeFileFinder(File fileRootDirInput, String fileName) throws Exception {
		List<String> pomList = new ArrayList<>(); 

		List<File> rootSubdirList = Arrays.asList(fileRootDirInput.listFiles((File dir) -> dir.isDirectory()));
		ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(rootSubdirList.size());
		List<Future<List<String>>> futureList = new ArrayList<>(); 
		
		for (File subdir : rootSubdirList) {
			Future<List<String>> future = newFixedThreadPool.submit(new FileFinder(subdir, fileName));
			futureList.add(future);
		}
		
		for (Future<List<String>> future : futureList) {
			while (!future.isDone()) {
				System.out.println("running...");
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		for (Future<List<String>> future : futureList) {
			List<String> list = future.get();
			pomList.addAll(list);
		}
		
		return pomList;
	}

	private String getBackupSuffix() {
		String suffix = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss-n"));
		return suffix;
	}

	private void backupFileList(List<String> fileList, String suffix) throws IOException {
		for (int i=0; i<fileList.size(); i++) {
			File file = new File(fileList.get(i));
			File parentFile = file.getParentFile();
			
			FileUtils.copyFile(file, new File(parentFile.getAbsolutePath() + "/" + file.getName() + "_" + suffix + ".backup"));
		}	
	}
	
}