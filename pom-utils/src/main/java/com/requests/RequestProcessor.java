package com.requests;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class RequestProcessor {

	public static void main(String[] args) throws Throwable {
		
		File inputFile = new File("C:/temp/requests-input.txt");
		File outputFile = new File("C:/temp/requests-output.txt");
		
		List<RequestDto> requestDtoList = getRequestDtoList(inputFile);
		for (int i = 0; i < requestDtoList.size(); i++) {
			RequestDto requestDto = requestDtoList.get(i);
			System.out.println(requestDto.toString() + "\n");
		}
		
		System.out.println("");
	}

	private static List<RequestDto> getRequestDtoList(File inputFile) throws Throwable {
		List<RequestDto> requestDtoList = new ArrayList<RequestDto>();
		
		List<String> lineListToProcess = getLineListToProcess(inputFile);
		for (int i = 0; i < lineListToProcess.size(); i++) {
			String line = lineListToProcess.get(i);
			
			RequestDto requestDto = new RequestDto();
			processLine(requestDto, line);
			requestDtoList.add(requestDto);
		}

		return requestDtoList;
	}
	
	private static List<String> getLineListToProcess(File inputFile) throws Throwable {
		List<String> lineList = new ArrayList<String>();
		
		List<String> lineListInput = FileUtils.getContent(inputFile);
		for (int i = 0; i < lineListInput.size(); i++) {
			String line = lineListInput.get(i);
			
			if (ignore(line)) {
				continue;
			}
			
			lineList.add(line);
		}
		
		return lineList;
	}

	private static boolean ignore(String line) {
		if (StringUtils.containsIgnoreCase(line, "tags.tiqcdn.com"                               ))
			return true;                                                                         
		if (StringUtils.containsIgnoreCase(line, "www.google-analytics.com"                      ))
			return true;                                                                         
		if (StringUtils.containsIgnoreCase(line, "www.google.com"                                ))
			return true;                                                                         
		if (StringUtils.containsIgnoreCase(line, "stats.g.doubleclick.net"                       ))
			return true;                                                                         
		if (StringUtils.containsIgnoreCase(line, "/assets/"                                      ))
			return true;                                                                         
		if (StringUtils.containsIgnoreCase(line, "apigwpro"                                      ))
			return true;                                                                         
		if (StringUtils.containsIgnoreCase(line, "data:image/"                                   ))
			return true;                                                                         
		if (StringUtils.containsIgnoreCase(line, ".html"                                         ))
			return true;
		if (StringUtils.containsIgnoreCase(line, "/scripts/"                                     ))
			return true;			
		if (StringUtils.containsIgnoreCase(line, "/preAnalise/getTipos"                          ))
			return true;
		if (StringUtils.containsIgnoreCase(line, "/preAnalise/getUF"                             ))
			return true;
		if (StringUtils.containsIgnoreCase(line, "/preAnalise/getMarcas/"                        ))
			return true;
		if (StringUtils.containsIgnoreCase(line, "/preAnalise/getAnoModeloPorMarca/"             ))
			return true;
		if (StringUtils.containsIgnoreCase(line, "/preAnalise/getModelosPorMarcaAnoCombustivel/" ))
			return true;
		if (StringUtils.containsIgnoreCase(line, "/preAnalise/verificarCpfCnpj/"                 ))
			return true;
		if (StringUtils.containsIgnoreCase(line, "/recuperar-cliente/dadosPreAnalise/"           ))
			return true;
		if (StringUtils.containsIgnoreCase(line, "/simulacao/getCabecalhoPost/"                  ))
			return true;
		if (StringUtils.containsIgnoreCase(line, "/simulacaoSeguro/getPermissaoExibirTela/"      ))
			return true;
		if (StringUtils.containsIgnoreCase(line, "/simulacaoSeguro/ofertas/"                     ))
			return true;
		
		return false;
	}

	private static void processLine(RequestDto requestDto, String line) {
//		System.out.println(line);
		
		setRequestUrl(requestDto, line);
		setDataBinary(requestDto, line);
	}
	
	private static void setRequestUrl(RequestDto requestDto, String line) {
		Pattern patternInput;
		Matcher matcherInput;
		
		patternInput = Pattern.compile("curl '([^']*)'", Pattern.CASE_INSENSITIVE);		
		matcherInput = patternInput.matcher(line);
		while (matcherInput.find()) {
			requestDto.setRequestUrl(matcherInput.group(1));
		}
	}	

	private static void setDataBinary(RequestDto requestDto, String line) {
		Pattern patternInput;
		Matcher matcherInput;
		
		patternInput = Pattern.compile("--data-binary '([^']*)'", Pattern.CASE_INSENSITIVE);		
		matcherInput = patternInput.matcher(line);
		while (matcherInput.find()) {
			requestDto.setDataBinary(matcherInput.group(1));
		}
	}	
	
}
