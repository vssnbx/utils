import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class MainTests {

	public static void main(String[] args) {
		
		Pattern pattern = Pattern.compile("S|N");
		Matcher matcher = pattern.matcher("N");
		System.out.println(matcher.matches());
		
		List<String> l = Arrays.asList("a", "b");
		
		Runnable r = () -> System.out.println("closing");
		
		System.out.println("stringStream.forEach");
		l.forEach(s -> System.out.println(s));
		System.out.println("stringStream.stream");
		l.stream().onClose(r).forEach(s -> System.out.println(s));
		
		Stream<String> stringStream = l.stream();
		stringStream.onClose(r);
		Stream<Double> doubleStream = stringStream.map(s -> Double.valueOf(s)); // Exception?
		doubleStream.onClose(r);
		
		System.out.println("stringStream");
		//stringStream.forEach(s -> System.out.println(s));
		System.out.println("doubleStream");
		doubleStream.forEach(d -> System.out.println(d.toString())); // Exception!
		
		System.out.println("...");
	}
	
}
