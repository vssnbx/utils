package com.pom_utils.util;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class PomUtils {
	
	public void processPomList(List<String> pomList, String version) {
		for (int i = 0; i < pomList.size(); i++) {
			String pom = pomList.get(i);
			processPom(pom, version);
		}
	}
	
	private void processPom(String fileInput, String version) {
		System.out.println("#############################################################################################");
		System.out.println("Iniciando:" + fileInput);
		
		try {
			Document document = getDocument(fileInput);
			
			setProjectVersion(document, version);
			setVivereParentVersion(document, version);
			setVivereDependenciesVersion(document, version);
			
			writeFile(fileInput, document);
			
		} catch (Exception e) {
			System.err.println("Erro|" + fileInput + "|" + e.getMessage());
			e.printStackTrace();
		}
	}

	private void setProjectVersion(Document document, String newVersion) throws Exception {
		System.out.println("[setProjectVersion]");
		
		Node nodeVersion = getNodeByXPath(document, "/project/version");
		changeNodeTextContent(nodeVersion, newVersion);
	}
	
	private void setVivereParentVersion(Document document, String version) throws Exception {
		System.out.println("[setVivereParentVersion]");
		
		MavenChildNode parent = getNodeByXPathAndGroupId(document, "/project/parent", "vivere");
		if (null == parent) {
			System.out.println("setVivereParentVersion|null");
		} else {
			System.out.println("setVivereParentVersion|" + 
					parent.getGroupId().getNodeName()    + ":" + parent.getGroupId().getTextContent()    + "|" + 
					parent.getArtifactId().getNodeName() + ":" + parent.getArtifactId().getTextContent() + "|" +
					parent.getVersion().getNodeName()    + ":" + parent.getVersion().getTextContent());
			
			if (StringUtils.containsIgnoreCase(parent.getGroupId().getTextContent(), "vivere.package")) {
				System.out.println("IGNORING [vivere.package]");
				return;
			}
			if (StringUtils.containsIgnoreCase(parent.getGroupId().getTextContent(), "vivere.framework")) {
				System.out.println("IGNORING [vivere.framework]");
				return;
			}
			
			changeNodeTextContent(parent.getVersion(), version);
		}
	}
	
	private void setVivereDependenciesVersion(Document document, String version) throws Exception {
		System.out.println("[setVivereDependenciesVersion]");
		
		List<MavenChildNode> dependencies = getChildNodeListByXPathAndGroupId(document, "/project/dependencies", "vivere");
		if (null == dependencies) {
			System.out.println("setVivereDependenciesVersion|null");
		} else {
			for (MavenChildNode dependency : dependencies) {
				System.out.println("setVivereDependenciesVersion|" + 
						dependency.getGroupId().getNodeName()    + ":" + dependency.getGroupId().getTextContent()    + "|" + 
						dependency.getArtifactId().getNodeName() + ":" + dependency.getArtifactId().getTextContent() + "|" +
						dependency.getVersion().getNodeName()    + ":" + dependency.getVersion().getTextContent());
				
				if (StringUtils.containsIgnoreCase(dependency.getGroupId().getTextContent(), "vivere.package")) {
					System.out.println("IGNORING [vivere.package]");
					continue;
				}

				if (StringUtils.containsIgnoreCase(dependency.getGroupId().getTextContent(), "vivere.framework")) {
					System.out.println("IGNORING [vivere.framework]");
					continue;
				}
				
				changeNodeTextContent(dependency.getVersion(), version);
			}
		}
	}
	
	private void changeNodeTextContent(Node node, String newValue) {
		if (null == node) {
			System.out.printf("changeNodeTextContent|node|null\n");
			return;
		}
		
		String oldVersionNodeValue = node.getTextContent();
		String newVersionNodeValue = newValue;
		node.setTextContent(newVersionNodeValue);
		
		System.out.printf("changeNodeTextContent|" +
				"node.getNodeName=[%1$s]|" +
				"oldVersionNodeValue=[%2$s]|" +
				"newVersionNodeValue=[%3$s]\n", 
				node.getNodeName(),
				oldVersionNodeValue, 
				newVersionNodeValue);
	}
	
	private Node getNodeByXPath(Document document, String xPathExpression) throws Exception {
		XPath xPath = getXPath();
		Node node = (Node) xPath.evaluate(xPathExpression, document, XPathConstants.NODE);
		return node;
	}
	
	private MavenChildNode getNodeByXPathAndGroupId(Document document, String xPathExpression, String groupId) throws Exception {
		MavenChildNode mavenChildNode = null;
		
		boolean changeIt = false;
		Node childItemGroupId = null;
		Node childItemArtifactId = null;
		Node childItemVersion = null;
		
		Node node = getNodeByXPath(document, xPathExpression);
		if (null == node) {
			System.out.printf("getNodeByXPathAndGroupId|node|null\n");
			return null;
		}
		
		for (int i = 0; i < node.getChildNodes().getLength(); i++) {
			Node item = node.getChildNodes().item(i);
			
			String itemNodeName = item.getNodeName();
			//System.out.println("Testing|" + childItemNodeName + "|" + childItem.getTextContent());
			if (itemNodeName.equals("groupId") &&
					StringUtils.containsIgnoreCase(item.getTextContent(), groupId)) {
				changeIt = true;
				childItemGroupId = item;
			} else if (itemNodeName.equals("artifactId")) {
				childItemArtifactId = item;
			} else if (itemNodeName.equals("version")) {
				childItemVersion = item;
			}
		}
		
		if (changeIt && childItemGroupId != null /*&& childItemArtifactId != null && childItemVersion != null*/) {
			mavenChildNode = new MavenChildNode(childItemGroupId, childItemArtifactId, childItemVersion);
			//childItemVersion.setTextContent(version);
		}
		
		return mavenChildNode;
	}
	
	private List<MavenChildNode> getChildNodeListByXPathAndGroupId(Document document, String xPathExpression, String groupId) throws Exception {
		List<MavenChildNode> mavenChildNodeList = new ArrayList<>();
		
		Node node = getNodeByXPath(document, xPathExpression);
		if (null == node) {
			System.out.printf("getChildNodeListByXPathAndGroupId|node|null\n");
			return null;
		}

		for (int i = 0; i < node.getChildNodes().getLength(); i++) {
			Node item = node.getChildNodes().item(i);
			if (item.getChildNodes().getLength() == 0) {
				continue;
			}
			
			boolean changeIt = false;
			Node childItemGroupId = null;
			Node childItemArtifactId = null;
			Node childItemVersion = null;
			
			for (int j = 0; j < item.getChildNodes().getLength(); j++) {
				Node childItem = item.getChildNodes().item(j);
				if (childItem.getChildNodes().getLength() == 0) {
					continue;
				}
				
				String childItemNodeName = childItem.getNodeName();
				//System.out.println("Testing|" + childItemNodeName + "|" + childItem.getTextContent());
				if (childItemNodeName.equals("groupId") &&
						StringUtils.containsIgnoreCase(childItem.getTextContent(), groupId)) {
					changeIt = true;
					childItemGroupId = childItem;
				} else if (childItemNodeName.equals("artifactId")) {
					childItemArtifactId = childItem;
				} else if (childItemNodeName.equals("version")) {
					childItemVersion = childItem;
				}
			}
			
			if (changeIt && childItemGroupId != null && childItemArtifactId != null && childItemVersion != null) {
				mavenChildNodeList.add(new MavenChildNode(childItemGroupId, childItemArtifactId, childItemVersion));
				//childItemVersion.setTextContent(version);
			}
		}	
		
		return mavenChildNodeList;
	}
		
	private XPath getXPath() {
		XPathFactory xPathFactory = XPathFactory.newInstance();
		XPath xPath = xPathFactory.newXPath();
		return xPath;
	}

	private Document getDocument(String fileInput) throws Exception {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();		
		Document document = documentBuilder.parse(new File(fileInput));
		return document;
	}
	
	private void writeFile(String fileInput, Document document) throws Exception {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
	    Transformer transformer = transformerFactory.newTransformer();
	    //transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.transform(
	    		new DOMSource(document), 
	    		new StreamResult(new FileOutputStream(new File(fileInput))));		
	}
	
}
