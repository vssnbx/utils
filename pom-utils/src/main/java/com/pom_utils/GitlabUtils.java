package com.pom_utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.pom_utils.util.GitlabHttp;

public class GitlabUtils {

	private static final Integer PAGE_COUNT = 9;
	private static final String TEMP_PATH = "C:/temp/gitlab-";
	
	public static void main(String[] args) throws Exception {
		GitlabUtils gitLabUtils = new GitlabUtils();
		
		gitLabUtils.writeGitCLone();
	}

	private void writeGitCLone() throws Exception {
		
		File file0 = new File(TEMP_PATH + "0.txt");
//		if (!file0.exists()) {
			writeFile0(file0);
//		}
		
		String file0Read = readFile0(file0);
		
		List<String> projectList = getProjectList(file0Read);
	
		writeFile1(projectList);
		
		writeFile2(projectList);
	}

	private String readFile0(File file0) throws Exception {
		StringBuilder sb = new StringBuilder();
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file0));
		char[] cbuf = new char[50000];
		while (bufferedReader.read(cbuf)>0) {
			sb.append(cbuf);
		}
		bufferedReader.close();
		
		return sb.toString();
	}
	
	private void writeFile0(File file0) throws Exception {
		GitlabHttp gitlabHttp = GitlabHttp.getInstance();
		gitlabHttp.begin();
		
		gitlabHttp.getLogin();
		gitlabHttp.postLogin();
		List<String> pageList = getPages(gitlabHttp);

		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file0));
		for (int i=0; i<pageList.size(); i++) {
			System.out.println(pageList.get(i));
			
			bufferedWriter.write(pageList.get(i));
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
		
		gitlabHttp.end();
	}

	private List<String> getPages(GitlabHttp gitlabHttp) throws Exception {
		List<String> pageList = new ArrayList<>();
		
		for (int i=1; i<=PAGE_COUNT; i++) {
			String page = gitlabHttp.getPage(i);
			pageList.add(page);
		}

		return pageList;
	}

	private List<String> getProjectList(String file0Read) {
		HashSet<String> projectSet = new HashSet<>();
		
		Pattern patternInput = Pattern.compile("<a[^>]*class=\"project\"[^>]*href=\"([^\"]*)\"", Pattern.CASE_INSENSITIVE);		
		Matcher matcherInput = patternInput.matcher(file0Read);
		while (matcherInput.find()) {
			projectSet.add("http://gitlab.intranet" + matcherInput.group(1));
		}
		
		ArrayList<String> projectList = new ArrayList<>(projectSet);
		Collections.sort(projectList);		
		
		return projectList;
	}

	private void writeFile1(List<String> projectList) throws Exception {
		File file1 = new File(TEMP_PATH + "1.txt");
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file1));
		for (int i=0; i<projectList.size(); i++) {
			bufferedWriter.write(projectList.get(i));
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
	}
	
	private void writeFile2(List<String> projectList) throws Exception {
		File file1 = new File(TEMP_PATH + "2.sh");
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file1));
		
		writeLine(bufferedWriter, "#!/bin/bash"); 
		writeLine(bufferedWriter, "git config --system core.longpaths true");
		bufferedWriter.newLine();
		 
		writeLine(bufferedWriter, "SUBDIR=$(date +%Y-%m-%d_%H-%M-%S_%N)");
		writeLine(bufferedWriter, "OUTPUTDIR=$(pwd)");     
		writeLine(bufferedWriter, "OUTPUTDIR=\"$OUTPUTDIR/$SUBDIR\""); 
		writeLine(bufferedWriter, "if [ ! -d \"$OUTPUTDIR\" ]; then");
		writeLine(bufferedWriter, "    echo \"Criando $OUTPUTDIR...\"");
		writeLine(bufferedWriter, "    mkdir \"$OUTPUTDIR\""); 
		writeLine(bufferedWriter, "fi");
		bufferedWriter.newLine();

		writeLine(bufferedWriter, "PROJECT_I=0");
		writeLine(bufferedWriter, "PROJECT_N="+projectList.size());
		bufferedWriter.newLine();
		
		List<String> ignoredProjects = Arrays.asList(
				"/financeira/architecture/",
				"/financeira/frontend/",
				"/financeira/incubator/",
				"/financeira/sisgestao/",
				"/training/",
				"financeira/python_automations",
				"/financeira/bpo/backend/vivere-app.cdc");
		
		int n = 0;
		String lastSubdir = "";
		for (int i=0; i<projectList.size(); i++) {
			String project = projectList.get(i);
			
			if (ignoredProjects.stream()
					.anyMatch(ignoredProject -> StringUtils.containsIgnoreCase(project, ignoredProject))) {
				continue;
			}
			
			String[] split = StringUtils.split(project, "/");
			String subdir = split[5];
			String projectName = split[split.length-1];
			
			if (!lastSubdir.equals(subdir)) {
				bufferedWriter.newLine();
				bufferedWriter.newLine();
				writeLine(bufferedWriter, "###############################################################################");
				writeLine(bufferedWriter, "printf  \"Criando $OUTPUTDIR/"+subdir+"...\"");
				writeLine(bufferedWriter, "mkdir \"$OUTPUTDIR/"+subdir+"\"");
				writeLine(bufferedWriter, "cd    \"$OUTPUTDIR/"+subdir+"\"");
				bufferedWriter.newLine();
				bufferedWriter.newLine();
				
				lastSubdir = subdir;
			}
			
			//writeLine(bufferedWriter, "echo \"PROJETO ["+ ++n +"] DE ["+projectList.size()+"]\"");
			writeLine(bufferedWriter, "PROJECT_I=$(( PROJECT_I + 1 )); echo \"PROJETO [$PROJECT_I] DE [$PROJECT_N]\"");
			
			writeLine(bufferedWriter, "git clone " + project + ".git");
		}

		bufferedWriter.close();
	}
	
	private void writeLine(BufferedWriter bufferedWriter, String line) throws Exception {
		bufferedWriter.write(line);
		bufferedWriter.newLine();		
	}
	
}
